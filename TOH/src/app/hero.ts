export class Hero {
    id: number;
    name: string;
    strength: number;
    healthpoints: number;
}