import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero'
import { HeroService } from '../hero.service'
import { MessageService } from '../message.service';

@Component({
  selector: 'app-hero-battle',
  templateUrl: './hero-battle.component.html',
  styleUrls: ['./hero-battle.component.css']
})
export class HeroBattleComponent implements OnInit {

  heroes: Hero[];
  selectedHeroes: Hero[] = [];
  errorMessage: string;
  hit: number;

  constructor(private heroService: HeroService, private messageService: MessageService) { }

  randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  getHeroes(): void{
    this.heroService.getHeroes().subscribe(heroes => this.heroes = heroes);
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  ngOnInit() {
    this.getHeroes();
  }

  select(hero: Hero): void{
    if(this.selectedHeroes.length == 2){
      this.selectedHeroes.shift()
    }
    this.selectedHeroes.push(hero)
  }

    battle(): void {
    if(this.selectedHeroes.length != 2){
      this.messageService.add('HeroService : You need to select two heroes to fight !');
    }
    else{
      (async () => { 
      this.messageService.add(`HeroFight : ${this.selectedHeroes[0].name} VS ${this.selectedHeroes[1].name}`);
      
      while(this.selectedHeroes[0].healthpoints > 0 && this.selectedHeroes[1].healthpoints > 0){
        
        await this.sleep(1000);
        this.hit = this.randomIntFromInterval(1,this.selectedHeroes[0].strength);
        this.messageService.add(`${this.selectedHeroes[0].name} hits ${this.selectedHeroes[1].name} with a 
        ${this.hit} hit`);

        this.selectedHeroes[1].healthpoints = this.selectedHeroes[1].healthpoints - this.hit;

        await this.sleep(1000);


        this.hit = this.randomIntFromInterval(1,this.selectedHeroes[1].strength);
        this.messageService.add(`${this.selectedHeroes[1].name} hits ${this.selectedHeroes[0].name} with a 
        ${this.hit} hit`)

        this.selectedHeroes[0].healthpoints = this.selectedHeroes[0].healthpoints - this.hit;        
      }
    
      if(this.selectedHeroes[0].healthpoints <= 0) {
        this.messageService.add(`${this.selectedHeroes[1].name} won !`)
      }
      else{
        this.messageService.add(`${this.selectedHeroes[0].name} won !`)
      }
    })()
    }
  }

}
