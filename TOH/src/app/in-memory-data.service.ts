import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';

@Injectable({
  providedIn: 'root'
})

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      {id: 1, name: 'Loïc', strength: 10, healthpoints : 100},
      {id: 2, name: 'Narco', strength : 3, healthpoints : 50},
      {id: 3, name: 'Dr Nice', strength : 4, healthpoints : 50},
      {id: 4, name: 'Celeritas', strength : 5, healthpoints : 50 },
      {id: 5, name: 'Magneta', strength : 6, healthpoints : 50 },
      {id: 6, name: 'RubberMan', strength : 6, healthpoints : 50 },
      {id: 7, name: 'Dynama', strength : 2, healthpoints : 50 },
      {id: 8, name: 'Dr IQ', strength : 1, healthpoints : 50 },
      {id: 9, name: 'Magma', strength : 7, healthpoints : 50 },
      {id: 10, name: 'Tornado', strength : 8, healthpoints : 50 }

    ];

    return {heroes}
  }

  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 1;
  }

  constructor() { }
}
